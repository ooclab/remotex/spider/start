# 如何开始参与

Spider 小组为 RemoteX 提供稳定可靠的数据服务。欢迎大家参与！

## 基本原则

社区相关：

1. **安全/隐私** : 除了 `实现网` 案例，所有 spider 默认设置为 private 权限，每个独立的 spider 代码仓库只允许相关的少数人查看源代码。避免代码被滥用。

开发相关：

1. **自治** : 每个独立的数据来源采集，创建一个独立的 git 仓库，内部完全自治。开发语言、架构设计、个人习惯，都不做要求。
2. **容器** : 每一个 spider 实例最终使用 Docker 技术统一运行、维护。


## 文档

### 旧

- https://wp.ooclab.org/projects/remotex/wiki
- https://wp.ooclab.org/projects/remotex/wiki/RemoteX-API
- https://wp.ooclab.org/projects/remotex-spider/wiki

### 新

[Wiki](https://gitlab.com/ooclab/remotex/spider/start/wikis/home)


## 联系我们

任何问题，欢迎联系我们：

| gitlab | nickname | email | others |
|--------|----------|-------|--------|
| @bryanlai | bryan 海明 | <bryan@echo2u.com> | |
| @gwind | 好风 | <lijian@ooclab.com> | QQ: 314859638 ; 微信: lijian_gnu |
| @p9s | mc | <mc.cheung@aol.com> | |
